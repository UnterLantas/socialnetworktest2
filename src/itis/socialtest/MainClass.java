package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        ArrayList<String> authorsData = new ArrayList<>();
        ArrayList<String> postsData = new ArrayList<>();
        File authorsFile = new File(authorsSourcePath);
        File postsFile = new File(postsSourcePath);
        try {
            FileReader fileReader = new FileReader(authorsFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String ph;
            while ((ph = bufferedReader.readLine()) != null){
                authorsData.add(ph);
            }

            FileReader fileReader1 = new FileReader(postsFile);
            BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
            String ph1;
            while ((ph1 = bufferedReader1.readLine()) != null){
                postsData.add(ph1);
            }
            fileReader.close();
            fileReader1.close();
            bufferedReader.close();
            bufferedReader1.close();

            ArrayList<Author> authorArrayList = new ArrayList<>();
            ArrayList<Post> postArrayList = new ArrayList<>();

            //Map<Integer, String> authorsMap = null;
            for (int i = 0; i < authorsData.size(); i++) {
                //authorsMap.put(Integer.valueOf(authorsData.get(i).substring(0,0)), authorsData.get(0).substring(3,authorsData.indexOf(',')));
                /*Long authorId = Long.valueOf(authorsData.get(i).substring(0, 1));
                authorsData.get(i).substring(3);
                String nickname = authorsData.get(i).substring(0, authorsData.indexOf(',') + 1 );
                authorsData.get(i).substring(0);
                String birthday = authorsData.get(i);
                authorArrayList.add(new Author(authorId, nickname, birthday));*/
            }

            for (int i = 0; i < postsData.size(); i++) {
                /*int authorId = Integer.parseInt(postsData.get(i).substring(0, postsData.indexOf(',')));
                Long likes = Long.parseLong(postsData.get(i).substring(postsData.indexOf(' '), postsData.indexOf('T')));
                String date = (postsData.get(i).substring(postsData.indexOf(' '), postsData.indexOf(',')));
                String content = postsData.get(i).substring(postsData.indexOf(' '));

                postArrayList.add(new Post(date, content, likes, authorArrayList.get(authorId)));*/
            }

            for (int i = 0; i < postArrayList.size(); i++) {
                /*System.out.println(postArrayList.get(i).getAuthor().getNickname()+" оставил комментарий с текстом: "+
                        postArrayList.get(i).getContent()+" и собрал "+postArrayList.get(i).getLikesCount()+" лайков. " +
                        "Пост оставлен "+postArrayList.get(i).getDate());*/
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
